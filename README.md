# Improving Primary Care Team Guide Drupal 8 Website

## Contents of this File

* Installation
* Maintainers


## Installation

### Clone the Repository

```
git clone git@bitbucket.org:ghalusa/ipctg-d8.git
```

### Install Drupal, Modules, and Themes

```
composer install
```

### Place Services and Settings Files

Unpack and place settings files into htdocs/sites/default. (Feel free to ask for copies of these files.)

- settings.php
- settings.local.php

### Set up database connections in `settings.local.php`

This is just an example.

```
/**
 * Database settings
 */
$databases['default']['default'] = array (
  'database' => 'db',
  'username' => 'db',
  'password' => 'db',
  'prefix' => '',
  'host' => 'mariadb',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
```

### Set the trusted host configuration in `settings.local.php`

These are just examples.

```
/**
 * Trusted host configuration.
 */
$settings['trusted_host_patterns'] = [
  '^your\.url\.path$',
  '^d8-primarycare\.ddev\.site$',
];
```

### Post Installation Checks

- Check the "Status report" page (admin/reports/status) and make sure everything is in order.
- Check the "Available updates" page (admin/reports/updates) to ensure core and modlues are up-to-date, which they should be.


## Maintainers/Support

* [Strategic Content Services](https://strategiccontent.com/)
* Goran Halusa: [ghalusa@gmail.com](ghalusa@gmail.com)